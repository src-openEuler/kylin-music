Name:           kylin-music
Version:        1.1.0.47
Release:        3
Summary:        kylin-music
License:        GPL-3.0-or-later and MIT
URL:            https://github.com/UbuntuKylin/kylin-music
Source0:        %{name}-%{version}.tar.gz

patch0:         0001-fix-compile-error-of-kylin-music.patch
patch1:		0001-Fix-version-number-of-about-dialog.patch
patch2:         0001-fix-kylin-music-build-error-about-ffmpeg7.1.patch
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qtchooser
BuildRequires:  qt5-qtscript-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  qt5-qtsvg-devel
BuildRequires:  qt5-linguist
BuildRequires:  qt5-qtmultimedia-devel
BuildRequires:  taglib-devel
BuildRequires:  gsettings-qt-devel
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  kf5-kwindowsystem-devel
BuildRequires:  ffmpeg-devel
BuildRequires:  ukui-interface libpeony3 libpeony-devel
BuildRequires:  mpv-libs-devel
BuildRequires:  sqlite-devel libXtst-devel
BuildRequires:  libkysdk-qtwidgets-devel
BuildRequires:  libkysdk-waylandhelper-devel

%description
kylin-music


%prep
%autosetup -n %{name}-%{version} -p1

%build
export PATH=%{_qt5_bindir}:$PATH
pushd kylin-music-plugins-simple
%cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr
%cmake_build
popd

mkdir qmake-build
pushd qmake-build
%{qmake_qt5} ..
%{make_build}
popd 

%install
export PATH=%{_qt5_bindir}:$PATH
pushd kylin-music-plugins-simple
%cmake_install
popd

pushd qmake-build
%{make_install} INSTALL_ROOT=%{buildroot}
popd 

mkdir -p %{buildroot}/usr/share/kylin-user-guide/data/guide

cp -r %{_builddir}/%{name}-%{version}/data/kylin-music %{buildroot}/usr/share/kylin-user-guide/data/guide/
%files
%doc debian/changelog
%license  debian/copyright 
%{_bindir}/*
%{_libdir}/libsimple.so
%{_datadir}/applications/kylin-music.desktop
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/pixmaps/kylin-music.png
%{_datadir}/kylin-music/
%{_datadir}/kylin-user-guide/data/guide/*

%changelog
* Wed Jan 15 2025 peijiankang <peijiankang@kylinos.cn> - 1.1.0.47-3
- update package version

* Mon May 27 2024 houhongxun <houhongxun@kylinos.cn> - 1.1.0.47-2
- fix the version number showed in about dialog

* Mon Apr 08 2024 houhongxun <houhongxun@kylinos.cn> - 1.1.0.47-1
- upgrade to upstream version 1.1.0.47-ok7.6
- use openeuler version number because it's higher than upstream

* Wed Jun 14 2023 huayadong <huayadong@kylinos.cn> - 1.1.3-3
- add Patch2：kylin-music-1.1.3_kylin_fix_install_the_repair_file_in_the_bin_directory.patch

* Wed Feb 1 2023 peijiankang <peijiankang@kylinos.cn> - 1.1.3-2
- add build debuginfo and debugsource

* Tue Jan 31 2023 peijiankang <peijiankang@kylinos.cn> - 1.1.3-1
- update version to 1.1.3

* Mon Aug 22 2022 peijiankang <peijiankang@kylinos.cn> - 1.1.2-6
- modify version is error

* Mon Aug 22 2022 peijiankang <peijiankang@kylinos.cn> - 1.1.2-5
- fix aarch64 install error

* Wed Aug 17 2022 peijiankang <peijiankang@kylinos.cn> - 1.1.2-4
- fix nothing provides libpthread.so.0

* Thu Jun 16 2022 peijiankang <peijiankang@kylinos.cn> - 1.1.2-3
- remove kylin-music_zh_CN.qm

* Tue Jun 14 2022 peijiankang <peijiankang@kylinos.cn> - 1.1.2-2
- add libsimple.so

* Tue Jun 14 2022 peijiankang <peijiankang@kylinos.cn> - 1.1.2-1
- update version to 1.1.2

* Tue Jun 7 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.44-6
- add kylin-user-guide file

* Wed May 18 2022 tanyulong<tanyulong@kylinos.cn> - 1.0.44-5
- Improve the project according to the requirements of compliance improvement

* Thu Apr 7 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.44-4
- modify version is error

* Wed Sep 08 2021 douyan <douyan@kylinos.cn> - 1.0.44-3
- fix_title_bar_issue.patch

* Mon Sep 06 2021 douyan <douyan@kylinos.cn> - 1.0.44-2
- add missing translation

* Thu Aug 19 2021 peijiankang <peijiankang@kylinos.cn> - 1.0.44-1
- Init kylin-music package for openEuler

